<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Theme extends Model {
	use SoftDeletes;

	protected $table = "themes";
	protected $softDelete = true;

	public function Lesson() {
		return $this->belongsTo('App\Lesson');
	}
	public function Formations() {
		return $this->belongsToMany('App\Formation', 'formation_theme');
	}
	public function Questions() {
		return $this->hasMany('App\Question');
	}

}
