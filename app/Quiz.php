<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model {
	public function User() {
		return $this->belongsTo('App\User');
	}
	public function Lessons() {
		return $this->belongsTo('App\Lesson');
	}
	public function Formation() {
		return $this->belongsTo('App\Formation');
	}
	public function Questions() {
		return $this->hasMany('App\Question');
	}
}
