<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends Model {

	use SoftDeletes;

	protected $table = "lessons";
	protected $softDelete = true;

	public function User() {
		return $this->belongsTo('App\User');
	}

	public function Formation() {
		return $this->belongsTo('App\Formation');
	}

	public function Personnels() {
		return $this->belongsToMany('App\Lesson', 'lesson_id');
	}

	public function Themes() {
		return $this->hasMany('App\Theme');
	}
}
