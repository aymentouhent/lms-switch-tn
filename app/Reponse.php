<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reponse extends Model {

	use SoftDeletes;
	protected $table = "reponses";
	protected $softDelete = true;

	public function Personnel() {
		return $this->belongsTo('App\User');
	}
	public function Choix() {
		return $this->belongsTo('App\Choix');
	}
}
