<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST')
        {
            // Update operation, exclude the record with id from the validation:
            $email_rule = 'email|unique:users,email,' . $this->get('id');
        }
        else
        {
            // Create operation. There is no id yet.
            $email_rule = 'email|unique:users,email';
        }
        return [
            'email'=>$email_rule
        ];
    }
}
