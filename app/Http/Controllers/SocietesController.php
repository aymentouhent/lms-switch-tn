<?php

namespace App\Http\Controllers;

use App\Formation;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\UploadsController;
use App\Societe;
use Illuminate\Support\Facades\Session;

class SocietesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $societe = Societe::all();
        return view('Back.Societe.index',compact('societe'));
    }


    public function create(){
        $formations = Formation::all();
        return view('Back.Societe.create',compact('formations'));

    }
    public function storeSociete(Request $request){
            $this->validate($request,[
               'nom' => 'required|max:255',
               'email' =>'required|unique:societes|max:255',
                'tel'=>'required|unique:societes'
            ]);
        $societe = new Societe();
        $societe->nom = $request->nom;
        $societe->adresse = $request->adresse;
        $societe->email = $request->email;
        $societe->tel = $request->tel;
        $societe->logo = UploadsController::upload('logo');
        $societe->save();
        $tab = array();
        if($request->formation_id == null){
            $societe->Formations()->sync($tab);
            return json_encode($societe);
        }
        if($request->formation_id) {
            for ($i = 0; $i < count($request->formation_id); $i++) {
                $manyToMany[$request->formation_id[$i]] = ['quota' => $request->quota [$i]];
            }
            $societe->Formations()->sync($manyToMany);
            return json_encode($societe);
        }

    }
    public function edit($id){
        $societe = Societe::find($id);
        $formations = Formation::all();
        $societe_formation = $societe->Formations()->get();
    $societe_responsable = $societe->Responsables()->where('role','=','Responsable')->get();
        $formations1= array();
        foreach ($formations as $formation){
            $formations1[$formation->id] = $formation->titre;
        }
        return view('Back.Societe.edit',compact('societe','formations1','formations','societe_formation','societe_responsable'));

    }

    public function updateSociete(Request $request,$id){

        $societe = Societe::find($id);
        $societe->nom = $request->nom;
        $societe->adresse = $request->adresse;
        $societe->email = $request->email;
        $societe->tel = $request->tel;
        if($request->hasFile('logo')){
            $societe->logo = UploadsController::upload('logo');
        }
        $societe->update();
        $tab = array();
        if($request->formation_id == null){
            $societe->Formations()->sync($tab);
            return json_encode($societe);
        }

        for ( $i=0 ; $i< count($request->formation_id); $i++ )
        {
            $manyToMany[ $request->formation_id[$i] ] = ['quota' => $request->quota [$i]];
        }
        $societe->Formations()->sync($manyToMany,false);
        return json_encode($societe);
    }

    public function deleteSociete($id){
        $societe = Societe::find($id);
        $societe->Formations()->detach();
        $societe->delete();
        return redirect()->route('index.societe');
    }
}
