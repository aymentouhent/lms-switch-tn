<?php

namespace App\Http\Controllers;

use App\Formation;
use App\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LessonsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $lessons = Lesson::all();
        return view('Back.Lesson.index',compact('lessons'));
    }


    public function create(){
        $formations = Formation::all();
        return view('Back.Lesson.create',compact('formations'));

    }


    public function upload(Request $request,$basename ='smartExpr-')
    {

        $file = $request->file('video');
        $filename = $basename.time() . '.' .$file->getClientOriginalExtension();
        $path = 'public/uploads/';
        $file->move($path,$filename);
        return $filename;
    }


    public function storeLesson(Request $request){
        $this->validate($request,array(
                'video'  => 'mimes:mp4,mov,ogg,qt | max:200000'
            )
        );
        $formation =  Formation::all()->last();
        $lesson = new Lesson();
        $lesson->video = $this->upload($request);
        $lesson->user_id = Auth::user()->id;
        $lesson->formation_id = $formation->id;
        $lesson->save();
        return json_encode($lesson);
    }
    public function edit($id){
        $lesson = Lesson::find($id);
        $formations =Formation::all();
        foreach ($formations as $formation) {
            $formationsArray[$formation->id] = $formation->titre;
        }
        return view('Back.Lesson.edit',compact('lesson','formationsArray'));

    }

    public function updateLesson(Request $request,$id,$id_formation){

        /*$this->validate($request,array(
                'video'  => 'mimes:mp4,mov,ogg,qt | max:200000'
            )
        );*/
        $formation = Formation::find($id_formation);
        $lesson = Lesson::find($id);
        if($request->hasFile('video')) {
            $lesson->video = $this->upload($request);
        }
        $lesson->user_id = Auth::user()->id;
        $lesson->formation_id = $formation->id;
        $lesson->update();
        Session::flash('success', 'Lesson modifié avec succés!');
        return redirect()->back();
    }

    public function deleteLesson($id){
        $lesson = Lesson::find($id);
        $lesson->delete();
        return redirect()->back();
    }
}