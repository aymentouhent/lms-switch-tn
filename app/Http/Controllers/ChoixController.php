<?php

namespace App\Http\Controllers;
use App\Choix;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ChoixController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}

	public function index() {
		$choix = Choix::all();
		return view('Back.Choix.index', compact('choix'));
	}

	public function create() {
		$questions = Question::all();
		return view('Back.Choix.create', compact('questions'));

	}
	public function storeChoix(Request $request, $id) {
		$request->validate([
			'libelle' => 'required|max:255',
		]);
		$question = Question::find($id);
		$choix = new Choix();
		$choix->libelle = $request->libelle;
		$choix->question_id = $question->id;
		//dd($question->id);
		$choix->save();
		Session::flash('success', 'Choix ajouté avec succés!');

		return redirect()->back();
	}
	public function edit($id) {
		$choix = Choix::find($id);
		return view('Back.Choix.edit', compact('choix'));

	}

	public function updateChoix(Request $request, $id) {

		$choix = Choix::find($id);
		$choix->libelle = $request->libelle;
		$choix->update();
		Session::flash('success', 'Choix modifié avec succés!');
		return redirect()->back();
	}

	public function deleteChoix($id) {
		$choix = Choix::find($id);
		$choix->delete();
		return redirect()->back();
	}
}
