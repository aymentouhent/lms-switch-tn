<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
class UploadsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    static public function upload($file, $basename ='smartExp-'){

        if(Input::file()){
            $image = Input::file($file);
            $filename  =$basename.time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('uploads/' . $filename);
            $image = Image::make($image->getRealPath());

            $image->resize(476,248);

            $image->save($path, 80);
            return $filename;
        }
    }


}
