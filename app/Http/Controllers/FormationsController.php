<?php

namespace App\Http\Controllers;

use App\Societe;
use App\Theme;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Formation;
use Illuminate\Support\Facades\Session;

class FormationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $societes = Societe::where('id',Auth::user()->societe_id)->get();
        foreach ($societes as $societe){
            $formations = $societe->Formations()->get();
        }

            $Formations = Formation::all();

        return view('Back.Formation.index',compact('array','formations','Formations'));
    }

    public function quiz(){
        return view('Front.Formation.quiz');
    }
    public function quiz1(){
        return view('Front.Formation.quiz1');
    }
    public function quiz1_certificate(){
        return view('Front.Formation.quiz_certificate');
    }
    public function quiz1_echoue(){
        return view('Front.Formation.quiz_echoue');
    }
    public function profil(){
        return view('Front.Formation.profil');
    }
    public function certifications(){
        return view('Front.Formation.certifications');
    }
    public function create(){
        $formation =  Formation::all()->last();
        $formation1 = new Formation();
        return view('Back.Formation.create',compact('formation','formation1'));

    }
    public function storeFormation(Request $request){
            $themes = Theme::all();
            if(count($themes) < 5) {
                $returnData = array(
                    'status' => 'erreur',
                    'message' => 'vous devez ajouter 5 thémes à l\'avance'
                );

                return Response::json($returnData, 500);
            }
                else{
                    $formation = new Formation();
                    $formation->titre = $request->titre;
                    $formation->admin_id = Auth::user()->id;
                    $formation->save();
                    foreach($themes as $theme) {
                        $formation->Themes()->sync([$theme->id], false);
                    }

                    return json_encode($formation);
                }


    }
    public function edit($id){
        $formation = Formation::find($id);
        $themes = Theme::all();
        $themes2 = array();
        foreach ($themes as $theme) {
            $themes2[$theme->id] = $theme->titre;
        }
        return view('Back.Formation.edit',compact('formation','themes2','themes'));

    }

    public function updateFormation(Request $request,$id){
        $formation = Formation::find($id);
        $formation->titre = $request->titre;
        $formation->admin_id = Auth::user()->id;
        $formation->update();

        if (isset($request->themes)) {
            $formation->Themes()->sync($request->themes);
        } else {
            $formation->Themes()->sync(array());
        }

        Session::flash('success', 'Formation modifié avec succés!');
        return redirect()->route('index.formation');

    }
    public function show($id)
    {
        $formation = Formation::find($id);
        $societes = Societe::where('id',Auth::user()->societe_id)->get();
            foreach ($societes as $societe) {
                $x=$societe->Formations()->find($formation->id);
                $y=$formation->Personnels()->where('societe_id',Auth::user()->societe_id)->get();
            }
        return view('Back.Formation.show',compact('x','formation','y'));
    }


    public function deleteFormation($id){
        $formation = Formation::find($id);
        $formation->Themes()->detach();
        $formation->delete();
        Session::flash('success','Formation supprimé avec succés');
        return redirect()->back();
    }


    public function lesson(){
        return view ('Front.Formation.index');
    }
}
