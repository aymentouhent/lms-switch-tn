<?php

namespace App\Http\Controllers;

use App\Theme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ThemesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $themes = Theme::with('Questions')->get();
        //dd($themes);
        return view('Back.Theme.index',compact('themes'));
    }

    public function create(){
        $themes = Theme::all();
        return view('Back.Theme.create',compact('themes'));

    }
    public function storeTheme(Request $request){
            $this->validate($request,[
                'titre' =>'required'
            ]);

        $theme = new Theme();
        $theme->titre = $request->titre;
        $theme->description = $request->description;

        $theme->save();
        Session::flash('success', 'Theme ajouté avec succés!');

        return redirect()->route('index.theme');
    }

    public function edit($id){
        $theme = Theme::find($id);
        return view('Back.Theme.edit',compact('theme'));

    }

    public function updateTheme(Request $request,$id){

        $theme = Theme::find($id);
        $theme->titre = $request->titre;
        $theme->description = $request->description;
        $theme->update();
        Session::flash('success', 'Theme modifié avec succés!');
        return redirect()->route('index.theme');
    }
    public function deleteTheme($id){
        $theme = Theme::find($id);
        $theme->delete();
        return redirect()->route('index.theme');
    }
}
