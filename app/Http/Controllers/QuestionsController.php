<?php

namespace App\Http\Controllers;

use App\Question;
use App\Theme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $question = Question::all();
        return view('Back.Question.index',compact('question'));
    }


    public function storeQuestion(Request $request,$id){
        $this->validate($request,[
            'libelle' => 'required'
        ]);
        $theme= Theme::find($id);
        $question = new Question();
        $question->libelle = $request->libelle;
        $question->theme_id = $theme->id;
        $request->validate([
            'libelle' => 'required|unique:questions|max:255',
        ]);
        $question->save();
        Session::flash('success', 'Question ajouté avec succés!');

        return redirect()->route('index.theme');
    }
    public function edit($id){
        $question = Question::find($id);
        return view('Back.Question.edit',compact('question'));

    }

    public function updateQuestion(Request $request,$id){

        $question = Question::find($id);
        $question->libelle = $request->libelle;
        $question->update();
        Session::flash('success', 'Question modifié avec succés!');
        return redirect()->back();
    }

    public function deleteQuestion($id){
        $question = Question::find($id);
        $question->delete();
        return redirect()->route('index.question');
    }
}
