<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model {

	use SoftDeletes;

	protected $table = "questions";
	protected $softDelete = true;

	public function Choixes() {
		return $this->hasMany('App\Choix');
	}
	public function Theme() {
		return $this->belongsTo('App\Theme');
	}
}
