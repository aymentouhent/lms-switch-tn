<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formation extends Model {
	use SoftDeletes;
	protected $table = "formations";
	protected $softDelete = true;

	public function Personnels() {
		return $this->belongsToMany('App\User', 'user_formation');
	}

	public function User() {}

	public function Admin() {
		return $this->belongsTo('App\User', 'user_id');
	}
	public function Lessons() {
		return $this->hasMany('App\Lesson');
	}
	public function Quizzes() {
		return $this->hasMany('App\Quiz');
	}
	public function Themes() {
		return $this->belongsToMany('App\Theme', 'formation_theme');
	}
	public function Societes() {
		return $this->belongsToMany('App\Societe', 'societe_formation')->withPivot('quota');
	}
}
