<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFormationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_formation', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date_entree')->nullable();
            $table->dateTime('date_sortie')->nullable();
            $table->double('score')->nullable();
            $table->double('temps_quiz')->nullable();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;

            $table->integer('formation_id')->unsigned();
            $table->foreign('formation_id')->references('id')->on('formations')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_formation');
    }
}
