<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocieteFormationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('societe_formation', function (Blueprint $table) {
            $table->increments('id');
            $table->double('quota')->nullable()->unsigned();
            $table->integer('societe_id')->unsigned();
            $table->foreign('societe_id')->references('id')->on('societes')->onDelete('cascade');
            $table->integer('formation_id')->unsigned();
            $table->foreign('formation_id')->references('id')->on('formations')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('societe_formation');
    }
}
