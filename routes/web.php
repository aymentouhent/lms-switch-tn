<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'SmartExpr'], function () {

    Route::group(['prefix' => 'admin','middleware'=>'admin-role'], function () {

        Route::get('/dashboard', ['uses' => 'UsersController@dashboard', 'as' => 'admin']);
        Route::group(['prefix' => 'formation'], function () {

            Route::get('/create', ['uses' => 'FormationsController@create', 'as' => 'create.formation']);
            Route::post('/create', ['uses' => 'FormationsController@storeFormation', 'as' => 'store.formation']);
            Route::post('/create/lesson', ['uses' => 'LessonsController@storeLesson', 'as' => 'store.lesson']);
            Route::get('/edit/{id}', ['uses' => 'FormationsController@edit', 'as' => 'edit.formation']);
            Route::put('/edit/{id}', ['uses' => 'FormationsController@updateFormation', 'as' => 'update.formation']);
            Route::get('/index', ['uses' => 'FormationsController@index', 'as' => 'index.formation']);
            Route::get('/index/{id}', ['uses' => 'FormationsController@deleteFormation', 'as' => 'delete.formation']);
        });

        Route::group(['prefix' => 'lesson'], function () {
            Route::get('/edit/{id}', ['uses' => 'LessonsController@edit', 'as' => 'edit.lesson']);
            Route::put('/edit/{id}/{formation_id}', ['uses' => 'LessonsController@updateLesson', 'as' => 'update.lesson']);
            Route::get('/index', ['uses' => 'LessonsController@index', 'as' => 'index.lesson']);
            Route::delete('/index/{id}', ['uses' => 'LessonsController@deleteLesson', 'as' => 'delete.lesson']);
        });
        Route::group(['prefix' => 'societe'], function () {
            Route::get('/create', ['uses' => 'SocietesController@create', 'as' => 'create.societe']);
            Route::post('/create', ['uses' => 'SocietesController@storeSociete', 'as' => 'store.societe']);
            Route::post('/create/responsable', ['uses' => 'UsersController@store', 'as' => 'store.responsable']);
            Route::get('/edit/{id}', ['uses' => 'SocietesController@edit', 'as' => 'edit.societe']);
            Route::put('/edit/{id}', ['uses' => 'SocietesController@updateSociete', 'as' => 'update.societe']);

            Route::put('/edit/responsable/{id_societe}', ['uses' => 'UsersController@update', 'as' => 'update.responsable']);

            Route::get('/index', ['uses' => 'SocietesController@index', 'as' => 'index.societe']);
            Route::get('/index/{id}', ['uses' => 'SocietesController@deleteSociete', 'as' => 'delete.societe']);
        });

        Route::group(['prefix' => 'theme'], function () {
            Route::get('/create', ['uses' => 'ThemesController@create', 'as' => 'create.theme']);
            Route::post('/create', ['uses' => 'ThemesController@storeTheme', 'as' => 'store.theme']);
            Route::get('/edit/{id}', ['uses' => 'ThemesController@edit', 'as' => 'edit.theme']);
            Route::put('/edit/{id}', ['uses' => 'ThemesController@updateTheme', 'as' => 'update.theme']);
            Route::get('/index', ['uses' => 'ThemesController@index', 'as' => 'index.theme']);
            Route::get('/index/{id}', ['uses' => 'ThemesController@deleteTheme', 'as' => 'delete.theme']);
        });
        Route::group(['prefix' => 'utilisateurs'], function () {
            Route::post('/store', ['uses' => 'UsersController@store', 'as' => 'store.responsable']);
            Route::delete('/delete/{id}', ['uses' => 'UsersController@delete', 'as' => 'delete.responsable']);
            Route::get('/index', ['uses' => 'UsersController@indexResponsables', 'as' => 'index.responsables']);
        });

        Route::group(['prefix' => 'quiz'], function () {
            Route::group(['prefix' => 'question'], function () {

                Route::post('/create/{id}', ['uses' => 'QuestionsController@storeQuestion', 'as' => 'store.question']);
                Route::get('/edit/{id}', ['uses' => 'QuestionsController@edit', 'as' => 'edit.question']);
                Route::put('/edit/{id}', ['uses' => 'QuestionsController@updateQuestion', 'as' => 'update.question']);
                Route::get('/index', ['uses' => 'QuestionsController@index', 'as' => 'index.question']);
                Route::get ('/index/{id}', ['uses' => 'QuestionsController@deleteQuestion', 'as' => 'delete.question']);

            });
                Route::group(['prefix' => 'choix'], function () {
                    Route::get('/create', ['uses' => 'ChoixController@create', 'as' => 'create.choix']);
                    Route::post('/create/{id}', ['uses' => 'ChoixController@storeChoix', 'as' => 'store.choix']);
                    Route::get('/edit/{id}', ['uses' => 'ChoixController@edit', 'as' => 'edit.choix']);
                    Route::put('/edit/{id}', ['uses' => 'ChoixController@updateChoix', 'as' => 'update.choix']);
                    Route::get('/index', ['uses' => 'ChoixController@index', 'as' => 'index.choix']);
                    Route::get('/index/{id}', ['uses' => 'ChoixController@deleteChoix', 'as' => 'delete.choix']);

                });


        });


        });
    Route::group(['prefix' => 'responsable','middleware'=>'responsable-role'], function () {
        Route::get('/index', ['uses' => 'FormationsController@index', 'as' => 'index.formation.responsable']);
        Route::get('/dashboard', 'ResponsableController@dashboard')->name('responsable.dashboard');
        Route::get('/show/{id}', 'FormationsController@show')->name('formation.show');

        Route::get('/index/formation-{id}', 'ImportsController@index')->name('personnels');
        Route::post('/index/formation-{id}', 'ImportsController@import')->name('import.users');
        Route::get('/uploads/{file}', 'ImportsController@download');
        Route::get('/send', 'ImportsController@sendMailsOrSms');

    });
    Route::get('initialise/{param}-{id}', ['uses'=>'ImportsController@createPassword','as'=>'create.password']);
    Route::put('initialise/{id}', ['uses'=>'ImportsController@initialisePassword','as'=>'store.password']);

    Route::get('/lesson', 'FormationsController@lesson')->name('lesson');
    Route::get('/quiz', 'FormationsController@quiz')->name('quiz');
    Route::get('/quiz1', 'FormationsController@quiz1')->name('quiz1');
    Route::get('/certificat', 'FormationsController@quiz1_certificate')->name('certificat');
    Route::get('/echoue', 'FormationsController@quiz1_echoue');
    Route::get('/profil', 'FormationsController@profil')->name('profil');
    Route::get('/certifications', 'FormationsController@certifications')->name('certifications');



});
