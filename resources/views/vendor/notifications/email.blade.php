@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# @lang('Whoops!')
@else
# @lang('Cher Utilisateur !')
@endif
@endif

Vous recevez cet e-mail, car nous avons reçu une demande de réinitialisation du mot de passe pour votre compte.

{{-- Action Button --}}
@isset($actionText)
<?php
switch ($level) {
case 'success':
	$color = 'green';
	break;
case 'error':
	$color = 'red';
	break;
default:
	$color = 'blue';
}
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
Réinitialiser
@endcomponent
@endisset

{{-- Outro Lines --}}

{{-- Salutation --}}
@lang('Si vous n\'avez pas demandé la réinitialisation du mot de passe, aucune autre action n\'est requise .')
<br>
@lang('Cordialement') .


{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
@lang(
    "Si vous rencontrez des difficultés pour cliquer sur le bouton \":actionText\", copiez et collez l'URL ci-dessous dans votre navigateur Web :\n".
    '[:actionURL](:actionURL)',
    [
        'actionText' => "Réinitialiser",
        'actionURL' => $actionUrl
    ]
)
@endcomponent
@endisset
@endcomponent

