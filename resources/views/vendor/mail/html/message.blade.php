@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            &copy; {{ date('Y') }} Smart Xpr. Tous les droits sont réservés.
        @endcomponent
    @endslot
@endcomponent
