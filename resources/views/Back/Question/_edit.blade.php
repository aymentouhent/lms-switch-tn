<div class="modal fade" id="modifier{{$question->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modifier Question</h4>
            </div>
            <div class="modal-body">
                {!! Form::model($question,['data-parsley-validate'=>'','route' => ['update.question',$question->id],'method'=>'PUT','role'=>'form']) !!}
                <div class="form-group">
                    <label>libelle :</label>
                    <input type="text" class="form-control" placeholder="Libelle" name="libelle" value="{{$question->libelle}}">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Modifier</button>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>