<div id="myModale{{$question->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p>Voulez vous vraiment supprimer la question : {{$question->libelle}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                <a href="{{route('delete.question',$question->id)}}" class="btn btn-danger" >Supprimer</a>
            </div>
        </div>
    </div>
</div>