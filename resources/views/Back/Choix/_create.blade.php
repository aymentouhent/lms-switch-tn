<div class="modal fade" id="modal-choix-ajout-{{$question->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ajouter Un Choix</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => ['store.choix',$question->id], 'data-parsley-validate' => '')) !!}
                <div class="form-group">
                    <label for="inputEmail3">libelle :</label>
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Libelle" name="libelle" required="required">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Ajouter</button>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>