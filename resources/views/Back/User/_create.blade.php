
        <div class="modal modal modal-info fade in fade" id="societe-modif{{$societe->id}}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Ajouter Responsable</h4>
                    </div>
                    {!! Form::open(array('route' => ['update.responsable',$societe->id], 'data-parsley-validate' => '','class'=>'edit-responsable','role'=>'form','method'=>'put')) !!}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label>Nom :</label>

                                    <input type="text" class="form-control" placeholder="Nom" name="nom" >
                                </div>
                                <div class="form-group">
                                    <label>Telephone :</label>
                                    <input type="text" class="form-control" placeholder="telephone" name="tel" >
                                </div>
                            </div>
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label>Email :</label>
                                    <input type="email" class="form-control" placeholder="Email" name="email" >
                                </div>
                                <div class="form-group">
                                    <label>Mot de passe :</label>
                                    <input type="password" class="form-control" placeholder="Mot De Passe" name="password">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" style="margin-right: 0px;">Modifier</button>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>