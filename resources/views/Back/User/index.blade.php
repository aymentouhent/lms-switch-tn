@extends('Back.main')
@section('stylesheets')
    <link rel="stylesheet" href="{{asset('SmartExpBack/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    @endsection
@section('content')
    <section class="content-header">
        <h1>
            Ajouter Utilisateur
        </h1>
    </section>


    <section class="content">
        <div class="box box-default">
            <div class="row">
                <div class="col-md-7"></div>
                <div class="col-md-5">
                    @include('Back.partials._messages')
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <form action="{{route('import.users',$formation->id)}}" method="post" enctype="multipart/form-data">
                        <div class="col-md-6">
                            {{csrf_field()}}
                            <input type="file" name="imported-file" required="required"/>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary" type="submit">Import</button>
                        </div>
                        <label>NB: Vous devez respectez le format suivant : {{ link_to_asset('uploads/exemple.csv', 'cliquer ici!') }}</label>
                    </form>

                </div>
            </div>
                <div class="box">
                <div class="box-body">
                @if(count($users))
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <td>nom</td>
                            <td>tel</td>
                            <td>email</td>
                        </tr>
                        </thead>
                        @foreach($formation->Personnels as $user)
                            <tr>
                                <td>{{$user->nom}}</td>
                                <td>{{$user->tel}}</td>
                                <td>{{$user->email}}</td>
                            </tr>
                        @endforeach
                    </table>
                @endif
            </div>
            </div>
            </div>
    </section>
@endsection
@section('javascripts')
    <script src="{{asset('SmartExpBack/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('SmartExpBack/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $('#example1').DataTable()
    </script>
@endsection
