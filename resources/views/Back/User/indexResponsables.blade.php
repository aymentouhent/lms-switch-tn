@extends('Back.main')
@section('stylesheets')
    <link rel="stylesheet" href="{{asset('SmartExpBack/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Liste Des Personnels
        </h1>
    </section>
            <br>
    <section>
        <a class="btn btn-primary" style="margin-left: 18px" href="{{ route('create.responsable') }}">
            Ajouter un utilisateur
        </a>
    </section>

    <section class="content">
        <div class="box box-default">
            <div class="box">
                <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped table-responsive display nowrap" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <td>nom</td>
                                <td>tel</td>
                                <td>email</td>
                            </tr>
                            </thead>
                            @foreach($responsables as $responsable)
                                <tr>
                                    <td>{{$responsable->nom}}</td>
                                    <td>{{$responsable->tel}}</td>
                                    <td>{{$responsable->email}}</td>
                                    <td>
                                        {!! Form::open(['route' => ['delete.choix', $responsable->id], 'method' => 'DELETE']) !!}
                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle btn-sm" style="margin-bottom: 0px" type="button" data-toggle="dropdown">Actions
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <!-- Trigger the modal with a button
                                                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Supprimer</button>
                                                    -->
                                                    <a href="{{route('edit.responsable',$responsable->id)}}" class="dropdown-item">Modifier</a>
                                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#myModal{{$responsable->id}}">Supprimer</a>

                                                </li>
                                            </ul>
                                        </div>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('javascripts')
    <script src="{{asset('SmartExpBack/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('SmartExpBack/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $('#example1').DataTable(
            {responsive: true}
        )
    </script>
@endsection
