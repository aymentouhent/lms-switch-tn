<html>
<head>
    @include('Back.partials._head')
</head>
<body class="skin-blue sidebar-mini" style="height: auto; min-height: 100%;">
<div class="wrapper">
    @include('Back.partials._header')
    @include ('Back.partials._menu')
    <div class="content-wrapper" style="min-height: 901px;">
        @yield('content')
    </div>
    @include('Back.partials._footer')
</div>
@include('Back.partials._javascripts')
</body>
</html>
