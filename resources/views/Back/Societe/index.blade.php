@extends('Back.main')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Liste des sociétés
        </h1>
    </section><br>
    <section>
        <a class="btn btn-primary" style="margin-left: 18px" href="{{ route('create.societe') }}">
            Ajouter une société
        </a>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- /.col -->
            <!-- /.row -->
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Nom</th>
                                <th>Adresse</th>
                                <th>Email</th>
                                <th>Tel</th>
                                <th>Logo</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($societe as $societe)
                                <tr>
                                    <td>{{$societe->nom}}</td>
                                    <td>{{$societe->adresse}}</td>
                                    <td>{{$societe->email}}</td>
                                    <td>{{$societe->tel}}</td>
                                    @if($societe->logo == null)
                                        <td></td>
                                    @else
                                    <td><img src="{{asset('uploads/'.$societe->logo)}}" width="40" height="40"></td>
                                    @endif
                                    <td class="">
                                        {!! Form::open(['route' => ['delete.societe', $societe->id], 'method' => 'DELETE']) !!}

                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle btn-sm" style="margin-bottom: 0px" type="button" data-toggle="dropdown">Actions
                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <!-- Trigger the modal with a button
                                                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Supprimer</button>
                                                    -->
                                                    <a href="{{route('update.societe',$societe->id)}}" class="dropdown-item">Modifier</a>
                                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#myModal{{$societe->id}}" >Supprimer</a>

                                                </li>
                                            </ul>
                                        </div>
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                                <div id="myModal{{$societe->id}}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Supprimer</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Voulez vous vraiment supprimer la societe : {{$societe->nom}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                <a href="{{route('delete.societe',$societe->id)}}" class="btn btn-primary" >Supprimer</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

@endsection