@extends('Back.main')
@section('stylesheets')
    <link rel="stylesheet" href="{{asset('SmartExpFront/css/jquery-toast-min.css')}}" type="text/css">

@endsection
@section('content')
<section class="content-header">
    <h1>
        Ajouter société
    </h1>
</section>
<section class="content">
    <div class="box box-default">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::open(array('route' => 'store.societe', 'data-parsley-validate' => '','files'=>true,'id'=>'ajout-societe')) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label>Nom :</label>

                        <input type="text" class="form-control"  placeholder="Nom" name="nom">
                    </div>

                    <div class="form-group">
                        <label>Adresse :</label>
                        <input type="text" class="form-control" placeholder="Adresse" name="adresse">
                    </div>

                    <div class="form-group">
                        <label>Email :</label>
                        <input type="text" class="form-control"placeholder="Email" name="email">
                    </div>

                    <div class="form-group">
                        <label>Tel :</label>
                        <input type="text" class="form-control"  placeholder="Tel" name="tel">
                    </div>

                    <div class="form-group">
                        <label>Logo :</label>
                        <input type="file" class="form-control" name="logo">
                    </div>

                    <div class="row" id="plus">
                        <div class="ajout">
                                <div class="form-group col-md-8 col-sm-12 col-12">
                                    <label for="inputEmail3">Formations :</label>
                                    <select name="formation_id[]" class="form-control">
                                        @foreach($formations as $formation)
                                        <option value="{{$formation->id}}">{{$formation->titre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-2 col-sm-12 col-12">
                                    <label>Quota :</label>
                                    <input name="quota[]" class="form-control" >

                                </div>
                        </div>
                        <div class="col-md-1 col-sm-12 col-12">
                            <input class="btn btn-success ajout-row" style="margin-top: 25px"  type="button" value="+">
                        </div>

                    </div>
                </div>
                <div class="box-footer" style="float:right">
                    <a class="btn btn-warning" href="{{ route('index.societe') }}">
                        Retour à la liste
                    </a>
                    <button type="submit" class="btn btn-success" id="btn-submit">Ajouter</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <button class="btn btn-success"  value="1" onclick="displayForm(this)">Ajouter/ Modifier responsable</button>
            </div>
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="row">
                        <div class="col-md-7"></div>
                        <div class="col-md-5">
                            @include('Back.partials._messages')
                        </div>
                        {!! Form::open(array('route' => ['store.responsable'], 'data-parsley-validate' => '','id'=>'edit-responsable','role'=>'form','method'=>'post')) !!}

                        <div class="col-md-12 col-sm-12 col-xs-12" style="display: none" id="add">
                            <div class="box-body" id="box">
                                <div class="col-md-6 col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Nom :</label>

                                        <input type="text" class="form-control" placeholder="Nom" name="nom">
                                    </div>
                                    <div class="form-group">
                                        <label>Telephone :</label>
                                        <input type="text" class="form-control" placeholder="telephone" name="tel">
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-12">
                                    <div class="form-group">
                                        <label>Email :</label>
                                        <input type="email" class="form-control" placeholder="Email" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label>Mot de passe :</label>
                                        <input type="password" class="form-control" placeholder="Mot De Passe" name="password">
                                    </div>
                                </div>
                                <div class="form-group pull-right">
                                    <button type="submit" class="btn btn-success" id="btn-responsable">Ajouter</button>
                                </div>
                            </div>



                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>


        </div>
    </div>

</section>

@endsection
@section('javascripts')
    <script src="{{asset('SmartExpFront/js/jquery-toast-min.js')}}"></script>
<script>
    $( '.ajout-row' ).click(function() {
        var x = $(".ajout").html();
        $("#plus").append("<div>"+x+"<div class=\"form-group col-md-2 col-sm-12 col-12\"><input class=\"btn btn-danger delete-row\" style=\"margin-top: 25px\"  type=\"button\" onclick=\"$(this).parent().parent().remove()\" value=\"-\"></div></div>");
    });

    $('.delete-row' ).click(function() {
        var x = $(".ajout").html();
        $("#plus").remove(x);
    });

    function displayForm(c) {
        if (c.value == "1") {
            $('#add').slideToggle();
        };

    }
    $("#ajout-societe").submit(function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        var formData = new FormData(this);

        var $btn = $('#btn-submit');
        $btn.button('loading');
        setTimeout(function () {
            $btn.button('reset');
        }, 1000);
        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function () {

                $.toast({
                    heading: 'Succés',
                    text: 'Societe ajouté avec succées',
                    position: 'top-right',
                    loaderBg: '#008000',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6,
                    bgColor:'#008000'
                });
                $('#btn-submit').css('pointer-events','none');
            },
           error: function () {
                $.toast({
                    heading: 'Erreur',
                    text: 'Erreur lors de l\'ajout',
                    position: 'top-right',
                    loaderBg: '#FF0000',
                    icon: 'danger',
                    bgColor :'#FF0000',
                    hideAfter: 3500,
                    stack: 6
                });

            }
        });
    });


    $("#ajout-responsable").submit(function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');
        var form = $(this);
        var method = form.attr('method');
        var $btn = $('#btn-responsable');
        $btn.button('loading');
        setTimeout(function () {
            $btn.button('reset');
        }, 1000);
        $.ajax({
            type: method,
            url: url,
            data: data,
            dataType: "json",
            success: function () {
                $.toast({
                    heading: 'Succés',
                    text: 'Responsable ajouté avec succées',
                    position: 'top-right',
                    loaderBg: '#008000',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6,
                    bgColor:'#008000'
                });
                $('#btn-responsable').css('pointer-events','none');
            },
            error: function () {
                $.toast({
                    heading: 'Erreur',
                    text: 'Erreur lors de l\'ajout',
                    position: 'top-right',
                    loaderBg: '#FF0000',
                    icon: 'danger',
                    bgColor :'#FF0000',
                    hideAfter: 3500,
                    stack: 6
                });

            }
        });
    });

</script>
@endsection
