@extends('Back.main')
@section('stylesheets')
    <link rel="stylesheet" href="{{asset('SmartExpFront/css/jquery-toast-min.css')}}" type="text/css">

@endsection
@section('content')
<section class="content-header">
    <h1>
        Modifier Société
    </h1>
</section>
<section class="content">
    <!-- bootstrap datepicker -->
       <div class="box box-default">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::model($societe,['data-parsley-validate'=>'','route' => ['update.societe',$societe->id],'method'=>'PUT','role'=>'form','id'=>'edit-societe']) !!}
                    <div class="box-body">
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label >Nom :</label>

                                <input type="text" class="form-control" placeholder="Nom" name="nom" value="{{$societe->nom}}" >
                            </div>
                            <div class="form-group">
                                <label >Adresse :</label>

                                <input type="text" class="form-control"  placeholder="Adresse" name="adresse" value="{{$societe->adresse}}">
                            </div>
                            <div class="form-group">
                                <label >Email :</label>

                                <input type="text" class="form-control"  placeholder="Email" name="email" value="{{$societe->email}}">
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label >Tel :</label>
                                <input type="text" class="form-control"  placeholder="Tel" name="tel" value="{{$societe->tel}}">
                            </div>

                            <div class="row" id="plus">
                                <div id="ajout">
                                    <div class="form-group col-md-7 col-sm-12 col-12">
                                        <label >Formations :</label>
                                        <select name="formation_id[]" class="form-control" >
                                            @foreach($formations as $formation)
                                                <option value="{{$formation->id}}">{{$formation->titre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2 col-sm-12 col-12">
                                        <label>Quota :</label>
                                        <input name="quota[]" class="form-control">
                                    </div>
                                    <div class="col-md-1 col-sm-12 col-12">
                                        <input class="btn btn-primary" style="margin-top: 25px" id="ajout-row" type="button" value="+">
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-success" style="float:right" id="btn-modif">modifier</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
       </div>




    <div class="box box-default">
           <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 ">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th>Formation</th>
                            <th>Quota</th>
                        </tr>
                        </thead>
                      <tbody>
                      @foreach($societe_formation as $formation)
                      <tr>
                              <td>{{$formation->titre}}</td>
                          <td>{{$formation->pivot->quota}}</td>
                      </tr>
                      @endforeach

                      </tbody>
                    </table>
                </div>
           </div>
    </div>

    <div class="box box-default">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <a href="{{route('update.responsable',$societe->id)}}" class="btn btn-info" data-toggle="modal" data-target="#societe-modif{{$societe->id}}">Ajouter/ Modifier responsable</a>

        </div>
           <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                   @include('Back.User._create')
               </div>

               <div class="col-md-12 col-sm-12 col-xs-12">
                   <div class="box">
                       <div class="box-header">
                           <h3 class="box-title">Responsables</h3>
                       </div>
                       <!-- /.box-header -->
                       <div class="box-body table-responsive no-padding">
                           <table class="table table-hover">
                               <thead>
                               <th>Nom</th>
                               <th>Email</th>
                               <th>Telephone</th>
                               <th>Actions</th>
                               </thead>
                               <tbody>
                               @foreach($societe_responsable as $responsable)
                                   <tr>
                                       <td>{{$responsable->nom}}</td>
                                       <td>{{$responsable->email}}</td>
                                       <td>{{$responsable->tel}}</td>
                                       <td class="">

                                           <div class="dropdown">
                                               <button class="btn btn-default dropdown-toggle btn-sm" style="margin-bottom: 0px" type="button" data-toggle="dropdown">Actions
                                                   <span class="caret"></span>
                                               </button>
                                               <ul class="dropdown-menu">
                                                   <li>
                                                       <!-- Trigger the modal with a button
                                                       <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Supprimer</button>
                                                       -->
                                                       <a href="{{route('update.responsable',$responsable->id)}}" class="dropdown-item" data-toggle="modal" data-target="#myModal-modif{{$responsable->id}}">Modifier</a>
                                                       <a href="#" class="dropdown-item" data-toggle="modal" data-target="#myModal-supp{{$responsable->id}}">Supprimer</a>

                                                   </li>
                                               </ul>
                                           </div>

                                       </td>
                                       <td>
                                           <div class="modal modal modal-info fade in fade" id="myModal-modif{{$responsable->id}}">
                                               <div class="modal-dialog">
                                                   <div class="modal-content">
                                                       <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                               <span aria-hidden="true">&times;</span></button>
                                                           <h4 class="modal-title">Modifier Responsable</h4>
                                                       </div>
                                                       {!! Form::model($responsable,['data-parsley-validate'=>'','route' => ['update.responsable',$responsable->societe_id],'method'=>'PUT','role'=>'form','class'=>'edit-responsable']) !!}
                                                       <div class="modal-body">
                                                           <div class="row">
                                                               <div class="col-md-6 col-6">
                                                                   <div class="form-group">
                                                                       <label>Nom :</label>

                                                                       <input type="text" class="form-control" placeholder="Nom" name="nom" value="{{$responsable->nom}}">
                                                                   </div>
                                                                   <div class="form-group">
                                                                       <label>Telephone :</label>
                                                                       <input type="text" class="form-control" placeholder="telephone" name="tel" value="{{$responsable->tel}}">
                                                                   </div>
                                                               </div>
                                                               <div class="col-md-6 col-6">
                                                                   <div class="form-group">
                                                                       <label>Email :</label>
                                                                       <input type="email" class="form-control" placeholder="Email" name="email" value="{{$responsable->email}}">
                                                                   </div>
                                                                   <div class="form-group">
                                                                       <label>Mot de passe :</label>
                                                                       <input type="password" class="form-control" placeholder="Mot De Passe" name="password">
                                                                   </div>
                                                               </div>
                                                           </div>
                                                       </div>
                                                       <div class="modal-footer">
                                                           <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                           <button type="submit" class="btn btn-primary" style="margin-right: 0px;">Modifier</button>
                                                       </div>
                                                       {!! Form::close() !!}
                                                   </div>
                                                   <!-- /.modal-content -->
                                               </div>
                                               <!-- /.modal-dialog -->
                                           </div>
                                       </td>
                                       <td>
                                           <div id="myModal-supp{{$responsable->id}}" class="modal fade" role="dialog">
                                               <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                       <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                           <h4 class="modal-title">Supprimer</h4>
                                                       </div>
                                                       {!! Form::open(['route' => ['delete.responsable',$responsable->id], 'method' => 'DELETE']) !!}
                                                       <div class="modal-body">
                                                           <p>Voulez vous vraiment supprimer le responsable : {{$responsable->nom}}</p>
                                                       </div>
                                                       <div class="modal-footer">
                                                           <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                           <button type="submit" class="btn btn-primary" >Supprimer</button>
                                                       </div>
                                                       {!! Form::close() !!}
                                                   </div>

                                               </div>
                                           </div>
                                       </td>
                                   </tr>
                               @endforeach


                               </tbody>
                           </table>
                       </div>
                       <!-- /.box-body -->
                   </div>
                   <!-- /.box -->
               </div>



           </div>
    </div>
</section>
@endsection
@section('javascripts')
    <script src="{{asset('SmartExpFront/js/jquery-toast-min.js')}}"></script>
    <script>
$( "#ajout-row" ).click(function() {
    var x = $("#ajout").html();
    $("#plus").append("<div>"+x+"<div class=\"form-group col-md-2 col-sm-12 col-12\"><input class=\"btn btn-danger delete-row\" style=\"margin-top: 25px\"  type=\"button\" onclick=\"$(this).parent().parent().remove()\" value=\"-\"></div></div>");
});
$('.delete-row' ).click(function() {
    var x = $(".ajout").html();
    $("#plus").remove(x);
});



function displayForm(c) {
    if (c.value == "1") {
        $('#add').slideToggle();
        $('#anotherAdd').slideToggle();
    };

}


$('.edit-societe').submit(function (e) {
    e.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
        var $btn = $('#btn-modif');
        $btn.button('loading');
        setTimeout(function () {
            $btn.button('reset');
        }, 1000);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
            type: "PUT",
            url: url,
            data: formData,
        success: function () {

            $.toast({
                heading: 'Succés',
                text: 'Societe modifié avec succées',
                position: 'top-right',
                loaderBg: '#008000',
                icon: 'success',
                hideAfter: 3500,
                stack: 6,
                bgColor:'#008000'
            });
            $('#btn-modif').css('pointer-events','none');
        },
        error: function () {
            $.toast({
                heading: 'Erreur',
                text: 'Erreur lors de la modification',
                position: 'top-right',
                loaderBg: '#FF0000',
                icon: 'danger',
                bgColor :'#FF0000',
                hideAfter: 3500,
                stack: 6
            });

        }
    });
});


/*$('#edit-responsable').submit(function (e) {
    e.preventDefault();
    var data = $(this).serialize();
    var url = $(this).attr('action');
    var $btn = $('#modif-responsable');
    $btn.button('loading');
    setTimeout(function () {
        $btn.button('reset');
    }, 1000);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "PUT",
        url: url,
        data: data,
        success: function () {

            $.toast({
                heading: 'Succés',
                text: 'Responsable modifié avec succées',
                position: 'top-right',
                loaderBg: '#008000',
                icon: 'success',
                hideAfter: 3500,
                stack: 6,
                bgColor:'#008000'
            });
            $('#modif-responsable').css('pointer-events','none');
        },
        error: function () {
            $.toast({
                heading: 'Erreur',
                text: 'Erreur lors de la modification',
                position: 'top-right',
                loaderBg: '#FF0000',
                icon: 'danger',
                bgColor :'#FF0000',
                hideAfter: 3500,
                stack: 6
            });

        }
    });
});
*/
</script>
@endsection
