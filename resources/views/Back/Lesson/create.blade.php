@extends('Back.main')
@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('SmartExpBack/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>

        Ajouter Lesson
    </h1>
</section>
<section class="content">
    <div class="box box-default">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {!! Form::open(array('route' => 'store.lesson', 'data-parsley-validate' => '','files' => true)) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputEmail3">Télécharger une video :</label>

                        <input type="file" class="form-control" name="video">
                    </div>

                </div>

                <div class="box-footer" style="float:right">
                    <a class="btn btn-primary" href="{{ route('index.lesson') }}">
                        Retour à la liste
                    </a>
                    <button type="submit" class="btn btn-primary" >Ajouter</button>

                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>
@endsection

