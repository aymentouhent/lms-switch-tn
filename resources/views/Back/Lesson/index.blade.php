@extends('Back.main')
@section('content')
<section class="content-header">
    <h1>
        List Des Leçons
    </h1>
</section>
<section class="content">
    <div class="box box-default">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @foreach($lessons as $lesson)
                <div>
                <video src="{{asset('public/uploads/'.$lesson->video)}}" type="video/mp4" width="400" controls >

                </video>
                    <br>
                    {!! Form::open(['route' => ['delete.lesson', $lesson->id], 'method' => 'DELETE']) !!}

                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

                    {!! Form::close() !!}
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
</div>
@endsection