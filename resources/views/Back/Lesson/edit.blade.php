@extends('Back.main')
@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('SmartExpBack/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Edit Lesson
    </h1>
</section>
<section class="content">
    <div class="box box-default">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {!! Form::model($lesson,['data-parsley-validate'=>'','route' => ['update.lesson',$lesson->id],'method'=>'PUT']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label>upload video :</label>
                        <input type="file" class="form-control" name="video">
                    </div>

                    <div class="form-group">
                        {{ Form::label('formation_id','Telecharger Video:',['class'=>'form-spacing-top']) }}
                        {{Form::select('formation_id',$formationsArray,null,array('class'=>'form-control'))}}
                    </div>

                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" style="float:right">Ajouter</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>
</div>
@endsection

