@extends('Back.main')
@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Liste des formations
            </h1>
        </section><br>
        <section>
            @if(Auth::user()->role == "superAdmin")
            <a class="btn btn-primary" style="margin-left: 18px" href="{{ route('create.formation') }}">
                Ajouter une formation
            </a>
                @endif
        </section>
        <!-- Main content -->
        @if(Auth::user()->role =="responsable")
        <section class="content">
            <div class="row">
                 @foreach($formations as $formation)

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box small-box bg-green">
                        <div class="inner">
                            <h2>Titre : {{$formation->titre}}</h2>

                            <p>Crée Le : {{$formation->created_at}}</p>

                            <p>Quota : {{$formation->pivot->quota}}</p>

                        </div>
                        <div class="icon">
                            <i class="fa fa-foursquare"></i>
                        </div>

                        <a href="{{route('formation.show',$formation->id)}}" class="small-box-footer">Plus d'informations</a>
                    </div>
                </div>
                        @endforeach


            </div>
        </section>
            @elseif(Auth::user()->role =="superAdmin")
            <section class="content">
                <div class="row">
                    <!-- /.col -->
                    <!-- /.row -->
                    <div class="col-xs-12">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Titre</th>
                                        <th>Actions</th>
                                    </tr>
                                    @foreach($Formations as $formation)
                                        <tr>
                                            <td>{{$formation->titre}}</td>
                                            <td class="">
                                                {!! Form::open(['route' => ['delete.question', $formation->id], 'method' => 'DELETE']) !!}
                                                <div class="dropdown">
                                                    <button class="btn btn-default dropdown-toggle btn-sm" style="margin-bottom: 0px" type="button" data-toggle="dropdown">Actions
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <!-- Trigger the modal with a button
                                                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Supprimer</button>
                                                            -->
                                                            <a href="{{route('update.formation',$formation->id)}}" class="dropdown-item">Modifier</a>
                                                            <a href="#" class="dropdown-item" data-toggle="modal" data-target="#myModal{{$formation->id}}">Supprimer</a>

                                                        </li>
                                                    </ul>
                                                </div>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                        <div id="myModal{{$formation->id}}" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Supprimer</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Voulez vous vraiment supprimer la formation : {{$formation->titre}}</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                        <a href="{{route('delete.formation',$formation->id)}}" class="btn btn-primary" >Supprimer</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @endforeach
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        @endif

        <!-- /.modal -->

@endsection