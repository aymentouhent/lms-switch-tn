@extends('Back.main')
@section('stylesheets')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" type="text/css">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Modifier Formation
    </h1>
</section>
<section class="content">
    <div class="box box-default">
        <div class="row">
            <div class="col-md-8">
                {!! Form::model($formation,['data-parsley-validate'=>'','route' => ['update.formation',$formation->id],'method'=>'PUT']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputEmail3">Titre :</label>

                        <input type="text" class="form-control" id="inputEmail3" placeholder="Titre" name="titre" value="{{$formation->titre}}">
                    </div>
                    {{ Form::label('themes', 'Themes:') }}
                    {{ Form::select('themes[]', $themes2, null, ['class' => 'form-control select2-multi', 'multiple' => 'multiple']) }}


                </div>
                <div class="box-footer" style="float:right">
                    <a class="btn btn-warning"  style="margin-right: 0px;" href="{{ route('index.formation') }}">
                        Retour à la liste
                    </a>
                    <button type="submit" class="btn btn-success" style="margin-right: 0px;">modifier</button>

                {!! Form::close() !!}

                </div>
            </div>

            <div class="col-md-4">
                @foreach($formation->Lessons as $lesson)
                    <video src="{{asset('public/uploads/'.$lesson->video)}}" type="video/mp4" width="400" controls></video>
                <br>
                   <div class="clearfix">
                        <div class="btn-group inline">
                            <a href="#modal-default" class="btn btn-success" data-toggle="modal">Modifier</a>
                        </div>
                       {!! Form::open(['route' => ['delete.lesson', $lesson->id], 'method' => 'DELETE']) !!}
                       <button type="submit" class="btn btn-danger">Supprimer</button>
                       {!! Form::close() !!}
                   </div>
            </div>

            <div class="modal fade" id="modal-default">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Modifier  Lessson</h4>
                        </div>
                        {!! Form::model($lesson,['data-parsley-validate'=>'','route' => ['update.lesson',$lesson->id,$formation->id],'method'=>'PUT']) !!}
                        <div class="modal-body">
                            <div class="form-group">
                                <label>upload video :</label>
                                <input type="file" class="form-control" name="video">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Modifier</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
                @endforeach
            </div>

        </div>


</section>
@endsection
@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.select2-multi').select2();
            $('.select2-multi').select2().val({!! json_encode($formation->themes()->allRelatedIds()) !!}).trigger('change');
        });
    </script>
@endsection
