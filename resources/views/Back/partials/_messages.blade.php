
@if(session()->has('error'))
 <div class="alert alert-success alert-dismissible" role="alert">
    <strong>Success :</strong> {{session()->get('success')}}
    </div>
@endif
@if(count($errors)>0)

<div class="alert alert-danger" role="alert">

    <strong>Errors:</strong><ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger alert-dismissible" role="alert">
        <strong>Erreur :</strong> {{session()->get('error')}}
    </div>
@endif

