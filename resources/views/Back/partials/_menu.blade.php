<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview">
            @if(Auth::user()->role =="superAdmin")
            <li><a href="{{ route('index.societe') }}"><i class="fa fa-industry"></i> <span>Gestion des sociétés</span></a></li>

            <li><a href="{{ route('index.formation') }}"><i class="fa fa-foursquare"></i><span>Gestion des formations</span></a></li></li>
            <li><a href="{{route('index.theme')}}"><i class="fa fa-book"></i> <span>Gestion des Quizzes</span></a></li>
                @else
            <li><a href="{{ route('index.formation.responsable') }}"><i class="fa fa-foursquare"></i><span>Gestion des formations</span></a></li></li>
                @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
