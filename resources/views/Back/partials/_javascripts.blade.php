{{Html::Script('SmartExpBack/bower_components/jquery/dist/jquery.min.js')}}
<!-- Bootstrap 3.3.7 -->
{{Html::Script('SmartExpBack/bower_components/bootstrap/dist/js/bootstrap.min.js')}}
<!-- FastClick -->
{{Html::Script('SmartExpBack/bower_components/fastclick/lib/fastclick.js')}}
<!-- AdminLTE App -->
{{Html::Script('SmartExpBack/dist/js/adminlte.min.js')}}
<!-- Sparkline -->
{{Html::Script('SmartExpBack/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}
<!-- jvectormap  -->
{{Html::Script('SmartExpBack/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}
{{Html::Script('SmartExpBack/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}
<!-- SlimScroll -->
{{Html::Script('SmartExpBack/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{Html::Script('SmartExpBack/dist/js/pages/dashboard2.js')}}
<!-- AdminLTE for demo purposes -->
{{Html::Script('SmartExpBack/dist/js/demo.js')}}

@yield('javascripts')