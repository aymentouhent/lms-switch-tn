@extends('Back.main')
@section('content')
<section class="content-header">
    <h1>
        Ajouter Theme
    </h1>
</section>


<section class="content">
    <div class="box box-default">
        <div class="row">
            <div class="col-md-7"></div>
            <div class="col-md-5">
                @include('Back.partials._messages')
            </div>
            <div class="col-md-8 col-md-offset-2">
                {!! Form::open(array('route' => 'store.theme', 'data-parsley-validate' => '')) !!}
                <div class="box-body">

                    <div class="form-group">
                        <label for="inputEmail3">Titre :</label>

                        <input type="text" class="form-control" id="inputEmail3" placeholder="Titre" name="titre">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3">Description :</label>

                        <textarea name="description" class="form-control"></textarea>
                    </div>

                </div>
                <div class="box-footer" style="float:right">
                    <a class="btn btn-warning" href="{{ route('index.theme') }}">
                        Retour à la liste
                    </a>
                    <button type="submit" class="btn btn-success">Ajouter</button>


                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>
@endsection
