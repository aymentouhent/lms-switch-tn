@extends('Back.main')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
    table {
        width: 50%;

    }
</style>
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Liste des Quizzes
        </h1>
    </section><br>
    <section>
        <a class="btn bg-blue btn-flat margin" style="margin-left: 18px" href="{{ route('create.theme') }}" >Ajouter un théme</a>

    </section>
    <!-- Main content -->
    <section class="content">
        <div id="accordion">
        @foreach($themes as $theme)

                <h3>{{$theme->titre}}</h3>
                <div>
                    <div class="text-center">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-success-{{$theme->id}}">
                          Modifier Thème
                        </button>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-theme-delete-{{$theme->id}}">
                           Supprimer Thème
                        </button>
                    </div>
                    @include('Back.Theme.edit')
                    @include('Back.Theme._delete')
                    <br>
                    <button class="btn btn-success margin" data-toggle="modal" data-target="#modal-question-ajout-{{$theme->id}}">Ajouter une question</button>
                    @include('Back.Question._create')
    <table border="0" >
                @foreach($theme->Questions as $question)
            <tr>
                <td><p>{{$question->libelle}}</p></td>

                <td>
                    <div class="dropdown">
                        <a class="btn btn-primary btn-sm" style="color: white"  type="button" data-toggle="dropdown">Actions
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#" data-toggle="modal" data-target="#modifier{{$question->id}}" class="dropdown-item">Modifier</a>
                                <a href="#" class="dropdown-item" data-toggle="modal" data-target="#myModale{{$question->id}}" >Supprimer</a>
                                <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal-choix-ajout-{{$question->id}}">Ajouter Choix</a>
                            </li>
                        </ul>
                    </div><br>
                </td>
            </tr>
        <tr>
            <td>
            <table border="0">
                @foreach($question->Choixes as $choix)
                <tr>
                    <td ><p>{{$choix->libelle}}</p></td>

                    <td>
                        <div class="dropdown">
                            <a class="btn btn-primary btn-sm" style="color: white"  type="button" data-toggle="dropdown">Actions
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#modifier-choix-{{$choix->id}}" class="dropdown-item">Modifier</a>
                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#supprimer-choix-{{$choix->id}}" >Supprimer</a>
                                </li>
                            </ul>
                        </div><br>
                    </td>
                </tr>
                    @include('Back.Choix._edit')
                    <div id="supprimer-choix-{{$choix->id}}" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"></h4>
                                </div>

                                <div class="modal-body">

                                    <p>Voulez vous vraiment supprimer le choix : {{$choix->libelle}}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                    <a href="{{route('delete.choix',$choix->id)}}" class="btn btn-danger" >Supprimer</a>
                                </div>
                            </div>

                        </div>
                    </div>

                @endforeach
            </table>
            </td>
        </tr>
            @include('Back.Question._edit')
            @include('Back.Question._delete')
            @include('Back.Choix._create')
        @endforeach
    </table>
   </div>
            @endforeach
        </div>

    </section>
@endsection
@section('javascripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#accordion" ).accordion({
                collapsible: true
            });
        } );

    </script>
@endsection