<div class="modal modal-danger fade" id="modal-theme-delete-{{$theme->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Supprimer Thème</h4>
            </div>
            <div class="modal-body">
                <p>Voulez vous vraiment supprimer le choix : {{$theme->titre}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <a href="{{route('delete.theme',$theme->id)}}" class="btn btn-outline pull-right" style="color: #ffffff;">Supprimer</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>