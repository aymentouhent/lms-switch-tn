@extends('Front.main')
@section('content')

    <section class="content">
        <div class="box box-default">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    {!! Form::model($user,['data-parsley-validate'=>'','route' => ['store.password',$user->id],'method'=>'PUT','role'=>'form']) !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3">Mot De Passe :</label>
                            <input type="password" class="form-control"  placeholder="password" name="password">
                        </div>
                    </div>
                    <div class="box-footer" style="float:right">
                        <button type="submit" class="btn btn-success">Enregistrer</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection

