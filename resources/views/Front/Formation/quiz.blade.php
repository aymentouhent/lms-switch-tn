@extends('Front.main')
@section('stylesheet')
    <style>
        .banner_area  a:active{
            border-bottom: 4px solid #ffffff;
        }
    </style>
@endsection

@section('content')
    <section class="banner_area">
        <div class="container">
            <div class="row" style="padding-top:18px">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h1 style="color: white;font-size: 32px !important;font-weight: 500 !important;">Quizz 1</h1>
                </div>
                <div class="col-md-6 text-right peronnalize-img" >
                    <img src="{{asset('SmartExpFront/images/logo-f.png')}}" >
                </div>


            </div>

        </div>
    </section>
    <div class="content-wrapper" style="min-height: 901px;">
        <section class="formation_block_area section_gap">
            <div class="container h-100" style="">
                <div class="row h-100 justify-content-center align-items-center">
                    <div class="col-md-8 offset-md-2">
                        <div class="card">
                            <div class="card-body text-center" style="padding: 2.25rem;!important;">
                                <h1 style="font-family: 'Roboto', sans-serif;color:#676767;font-size: 36px">QUIZZ 1</h1>

                                <p  class="card-text" style="font-size: 16px;font-family: 'Roboto', sans-serif;color: #676767">
                                    Lorem ipsum dolor sit amet, et melius scriptorem usu. Ut mea al
                                    Lorem ipsum dolor sit amet, et melius scriptorem usu. Ut mea al
                                    Lorem ipsum dolor sit amet, et melius scriptorem usu. Ut mea al
                                    Lorem ipsum dolor sit amet, et melius scriptorem usu. Ut mea al
                                    Lorem ipsum dolor sit amet, et melius scriptorem usu. Ut mea al

                                </p>
                                <h4 style="font-size: 18px;font-family: 'Roboto', sans-serif;color: #676767;text-align: left">Nombre de questions : 10</h4>
                                <h4 style="font-size: 18px;font-family: 'Roboto', sans-serif;color: #676767;text-align: left">Temps : 10 min</h4>
                                <a href="{{route('quiz1')}}" class="btn btn-primary" style="background-color: #13c5fc;border: #13c5fc!important;border-radius: 0px;font-family: 'Roboto', sans-serif;font-size: 16px">Démarrer le quiz
                                    <span style="margin-left: 10px;" class="fa fa-chevron-right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endsection