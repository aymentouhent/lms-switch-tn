@extends('Front.main')
@section('stylesheet')
    <style>
        .banner_area  a:active{
            border-bottom: 4px solid #ffffff;
        }

    </style>
@endsection

@section('content')
    <section class="banner_area">
        <div class="container">
            <div class="row" style="padding-top:18px">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h1 style="color: white;font-size: 32px !important;font-weight: 500 !important;">Mon Compte</h1>
                </div>
                <div class="col-md-6 text-right peronnalize-img" >
                    <img src="{{asset('SmartExpFront/images/logo-f.png')}}" >
                </div>
                <div class="col-md-6 ">
                    <ul id="menu" style=" margin-left: -64px;">
                        <li class="active"><a href="#" style="color: #ffffff">Accueil </a></li>

                        <li><a href="#"  style="color: #ffffff">profil</a></li>
                        <li><a href="#" style="color: #ffffff">Certifications</a></li>
                    </ul>
                </div>

            </div>

        </div>
    </section>
    <div class="content-wrapper" style="min-height: 901px;">
        <section class="formation_block_area section_gap">
            <div class="container h-100" >
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <h2 style="font-family: 'Roboto', sans-serif;font-size: 20px;color: #676767;font-weight: bold">Informations Personnelles</h2>
                        <hr>
                        <div class="row">

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label style="color: #676767!important;">Nom</label>
                                    <input type="text" class="form-control" name="nom">
                                </div>
                                <div class="form-group">
                                    <label style="color: #676767!important;">Prénom</label>
                                    <input type="text" class="form-control" name="prenom">
                                </div>
                                <div class="form-group">
                                    <label style="color: #676767!important;">Date Naissance</label>
                                    <input type="text" class="form-control" name="date">
                                </div>
                                <div class="form-group">
                                    <label style="color: #676767!important;">Adresse</label>
                                    <input type="text" class="form-control" name="adresse">
                                </div>
                            </div>
                            <div class="col-md-2"></div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label style="color: #676767!important;">Telephone</label>
                                    <input type="text" class="form-control" name="tel">
                                </div>
                                <div class="form-group">
                                    <label style="color: #676767!important;">Email</label>
                                    <input type="text" class="form-control" name="prenom">
                                </div>
                                <div class="form-group">
                                    <label style="color: #676767!important;">Mot de passe</label>
                                    <input type="text" class="form-control" name="date">
                                </div>
                                <div class="form-group">
                                    <label style="color: #676767!important;">Confirmer le mot de passe</label>
                                    <input type="text" class="form-control" name="adresse">
                                </div>
                            </div>
                        </div>

                        <hr>

                            <button  style="background-color: #d3d3d3;border: #d3d3d3;border-radius: 0%!important;width: 255px;height: 52px;text-align: center;color: #ffffff">Retour</button>
                        <button  style="background-color: #11b1e2;border: #11b1e2;border-radius: 0%!important;width: 255px;height: 52px;text-align: center;color: #ffffff;float: right">Mettre a jour <i class="fa fa-chevron-right"></i></button>



                        </div>

                    </div>

                </div>
        </section>
    </div>
@endsection