@extends('Front.main')
@section('stylesheet')
    <style>
        .banner_area  a:active{
            border-bottom: 4px solid #ffffff;
        }
        .text-1{
            text-align: right;
        }
        .text-2{
            text-align: center;
        }
        .text-3{
            text-align: left;
        }
        @media only screen and (max-width: 1000px) {
            .text-1{
                text-align: center;
            }
            .text-2{
                text-align: center;
            }
            .text-3{
                text-align: center;
            }
        }
    </style>
@endsection

@section('content')
    <section class="banner_area">
        <div class="container">
            <div class="row" style="padding-top:18px">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h1 style="color: white;font-size: 32px !important;font-weight: 500 !important;">Quizz 1</h1>
                </div>
                <div class="col-md-6 text-right peronnalize-img" >
                    <img src="{{asset('SmartExpFront/images/logo-f.png')}}" >
                </div>


            </div>

        </div>
    </section>
    <div class="content-wrapper" style="min-height: 901px;">
        <section class="formation_block_area section_gap">
            <div class="container h-100" style="">
                <div class="row ">
                    <div class="col-md-8 col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body" style="padding: 2.25rem;!important;background-color: #13c5fc">
                                
                                <h5 style="color:#97e6fe;display: inline-block;font-family: 'Roboto', sans-serif;font-size: 12px">Formation1 > </h5>
                                <h4 style="font-family: 'Roboto', sans-serif;font-size: 18px;color: #ffffff;text-align: left!important;display: inline-block"> Quizz1</h4>

                                <div class="progress"  style="background-color: #0f9eca!important">
                                    <div class="progress-bar" role="progressbar" style="width: 100%;background-color: #ffffff" aria-valuenow="25" aria-valuemin="48" aria-valuemax="100" ></div>
                                </div>
                                <h2 class="text-center" style="color: #ffffff;font-size: 32px;font-family: 'Roboto', sans-serif;font-weight: bold;padding-top: 14px;">100%</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body text-center" style="padding: 2.25rem;!important; ">
                                <h4 style="color: #676767;font-family: 'Roboto', sans-serif;font-size: 20px">Temps restants : </h4>
                                <h2 style="color: #676767;font-family: 'Roboto', sans-serif;font-size: 50px">03:22</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="br" style="margin-top: 60px;!important;"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-body" style="padding: 2.25rem">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <h3 class="text-center" style="color:#676767; font-family: 'Roboto', sans-serif;font-size: 24px">Vous venez de terminer votre Quizz</h3>
                                    </div>
                                </div>
                                <div class="br" style="margin-top: 14px;!important;"></div>
                                <div class="row">
                                    <div class="col-md-2 "></div>
                                    <div class="col-md-2 text-1 col-12">
                                        <h3 style="color:#676767; font-family: 'Roboto', sans-serif;font-size: 24px;font-weight: bold;display: inline-block">1/2<br> Tentatives
                                        </h3>
                                    </div>

                                    <div class="col-md-3 text-2 col-12">
                                        <h3 style="color:#676767; font-family: 'Roboto', sans-serif;font-size: 24px;font-weight: bold;display: inline-block">16/20<br> Meilleur Score
                                        </h3>
                                    </div>

                                    <div class="col-md-3 text-3 col-12">
                                        <h3 style="color:#676767; font-family: 'Roboto', sans-serif;font-size: 24px;font-weight: bold;display: inline-block">16/20<br> Score Finale</h3>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="br" style="margin-top: 14px;!important;"></div>
                                <div class="col-md-12 col-12">
                                    <h1 class="text-center" style="color:#676767; font-family: 'Roboto', sans-serif;font-size: 30px;font-weight: bold">Certification réussie <img src="{{asset('SmartExpFront/image/cetificat.png')}}"></h1>
                                </div>
                             </div>

                         </div>
                        <div class="br" style="margin-top: 60px;!important;"></div>
                        <div class="form-group text-center">
                            <a href="{{route('certifications')}}" class="btn btn-primary" style="background-color: #13c5fb;border: #13c5fb;border-radius: 0%!important;width: 263px;height: 64px;text-align: center;padding-top:16px"><i class="fa fa-chevron-left" style="    float: left;padding-top: 6px;"></i> Retour à la formation</a>
                        </div>
                     </div>
                    </div>
                </div>
        </section>
    </div>
@endsection