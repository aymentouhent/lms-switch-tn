@extends('Front.main')
@section('stylesheet')
    <style>
        .banner_area  a:active{
            border-bottom: 4px solid #ffffff;
        }

        input[type="radio"] {
            -webkit-appearance: checkbox;
            -moz-appearance: checkbox;
            background-color: #13c5fc;
        }

        .radio{
            border: 1px solid #d8d8d8;
            margin: 2px;
            width: 40px;
            height: 40px;
            position: relative;
            border-radius: 40%;
        }
        .radio-margin{
            margin-left: 470px;
        }
        @media only screen and (max-width: 1000px) {
            .radio-margin{
                margin-left: 59px;
            }
        }
        @media only screen and (max-width: 1000px) {
            .btn-question {
                padding-bottom: 20px;
                text-align: center;
            }
        }

    </style>
@endsection

@section('content')
    <section class="banner_area">
        <div class="container">
            <div class="row" style="padding-top:18px">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h1 style="color: white;font-size: 32px !important;font-weight: 500 !important;">Quizz 1</h1>
                </div>
                <div class="col-md-6 text-right peronnalize-img" >
                    <img src="{{asset('SmartExpFront/images/logo-f.png')}}" >
                </div>


            </div>

        </div>
    </section>
    <div class="content-wrapper" style="min-height: 901px;">
        <section class="formation_block_area section_gap">
            <div class="container h-100" style="">
                <div class="row ">
                    <div class="col-md-8 col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body text-center" style="padding: 2.25rem;!important;background-color: #13c5fc">
                                <h4 style="font-family: 'Roboto', sans-serif;font-size: 18px;color: #ffffff;text-align: left!important;">Quizz1</h4>

                                <div class="progress"  style="background-color: #0f9eca!important">
                                    <div class="progress-bar" role="progressbar" style="width: 48%;background-color: #ffffff" aria-valuenow="25" aria-valuemin="48" aria-valuemax="100" ></div>
                                </div>
                                <h2 class="text-center" style="color: #ffffff;font-size: 32px;font-family: 'Roboto', sans-serif;font-weight: bold;padding-top: 14px;">48%</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body text-center" style="padding: 2.25rem;!important; ">
                                    <h4 style="color: #676767;font-family: 'Roboto', sans-serif;font-size: 20px">Temps restants : </h4>
                                    <h2 style="color: #676767;font-family: 'Roboto', sans-serif;font-size: 50px">09:37</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="br" style="margin-top: 60px;!important;"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-body" style="padding: 2.25rem">
                                <h4 style="font-family: 'Roboto', sans-serif;font-size: 16px;color: #aaaaaa;text-align: left!important;">Question 4/10</h4>
                                <h3 class="text-center" style="color:#676767; font-family: 'Roboto', sans-serif;font-size: 24px">Lorem ipsum dolor sit amet, duo vocent minimum probatus ne ?</h3>


                                <div class="form-check form-check-inline radio-margin">
                                    <input class="form-check-input radio" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="">
                                    <label class="form-check-label" for="inlineRadio1" style="color: #5b5b5b;font-family: 'Roboto', sans-serif;font-size: 24px;margin-bottom: 30px;">Oui</label>
                                </div>
                                <div class="form-check form-check-inline radio-margin">
                                    <input class="form-check-input radio" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="">
                                    <label class="form-check-label" for="inlineRadio2" style="color: #d8d8d8;font-family: 'Roboto', sans-serif;font-size: 24px;margin-bottom: 30px;">Non</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="br" style="margin-top: 60px;!important;"></div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 btn-question">
                                <button class="btn btn-primary text-left" style="background-color: #13c5fb;border: #13c5fb;border-radius: 0%!important;width: 182px;height: 48px;">
                                    <i class="fa fa-chevron-left"></i>
                                    <span style="font-size: 16px;font-family: 'Roboto', sans-serif">Question Précédent</span>
                                </button>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 btn-question text-right">
                                <a href="{{route('certificat')}}" class="btn btn-primary text-right" style="background-color: #13c5fb;border: #13c5fb;border-radius: 0%!important;width: 182px;height: 48px;padding-top:16px">
                                    <span style="font-size: 16px;font-family: 'Roboto', sans-serif">Question suivante  </span><i class="fa fa-chevron-right"></i>
                                </a>
                     </div>

                </div>
            </div>
        </section>
    </div>
@endsection