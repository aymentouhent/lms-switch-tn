@extends('Front.main')
@section('stylesheets')
    <style>
      video{  width: 100%;
         height: auto;}
      .fa {
          display: inline-block;
          font: normal normal normal 27px/1 FontAwesome;
          font-size: inherit;
          text-rendering: auto;
          -webkit-font-smoothing: antialiased;

          /* text-align: center; */
          -moz-osx-font-smoothing: grayscale;
      }
    </style>
    @endsection
@section('content')
    <section class="banner_area">
        <div class="container">
            <div class="row" style="padding-top:18px">
                <div class="col-md-6">
                    <h1  style="color: white;font-size: 32px !important;font-weight: 500 !important;">Lorem ipsum dolor sit amet, te mea</h1>
                </div>
                    <div class="col-md-6 text-right peronnalize-img" >
                        <img src="{{asset('SmartExpFront/images/logo-f.png')}}" >
                    </div>

            </div>

        </div>
    </section>

    <section class="blog_area single-post-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-4 col-xs-12 posts-list">
                    <div class="single-post row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                                <video src="{{asset('public/uploads/smartExpr-1530279429.mp4')}}" type="video/mp4"   controls ></video>
                        </div>
                    </div>

                    <div class="comments-area">
                        <h4 style="font-family: 'Roboto', sans-serif;font-size: 18px;margin-bottom: 5px!important;">Lorem Ipsum ....</h4>
                        <div class="progress" style="background-color: #0f9eca!important;margin-bottom: 10px!important;">
                            <div class="progress-bar" role="progressbar" style="width: 35%;background-color: #ffffff;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" ></div>
                        </div>


                       <div>
                           <p style="font-family: 'Roboto', sans-serif;font-size: 18px;text-align: center;margin-bottom: 1px!important;color: #ffffff;font-weight: bold;">32%</p>
                           <p style="font-size: 18px;font-family: 'Roboto', sans-serif;text-transform: uppercase;text-align: center;color: #ffffff;font-weight: bold;">Progré de la formation</p>
                       </div>

                        <div>
                            <p style="font-family: 'Roboto', sans-serif;font-size: 14px;text-align: center;margin-bottom: 1px!important;display: inline-block">Date début : </p>
                            <p style="font-size: 14px;font-family: 'Roboto', sans-serif;text-transform: uppercase;text-align: center;color: #ffffff;display: inline-block;">18 juillet 2018</p>
                        </div>
                        <div>
                            <p style="font-family: 'Roboto', sans-serif;font-size: 14px;text-align: center;margin-bottom: 1px!important;display: inline-block">Temps : </p>
                            <p style="font-size: 14px;font-family: 'Roboto', sans-serif;text-align: center;color: #ffffff;display: inline-block;">18 heures</p>
                        </div>
                    </div>
                    <button class="btn btn-primary cours_btn"> <i class="fa fa-chevron-left"></i> Cours précédant</button>

                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 course">
                    <div class="blog_right_sidebar">
                       <div style="padding: 0px 30px;">
                           <p style="font-size: 12px;font-family: 'Roboto', sans-serif;display: inline-block">Course</p>
                           <p style="font-size: 12px;font-family: 'Roboto', sans-serif;display: inline-block;float: right;">1/5</p>
                           <p style="color: #000;font-family: 'Roboto', sans-serif;font-size: 18px">Lorem Ipsum ....</p>
                       </div>

                        <div class="row_1">

                                <p class="widget_title title screen"><span class="fa fa-check 2x" style="color: #13c5fc"></span> Lesson 1</p>
                                <p class="widget_title title">3/3</p>

                        </div>

                        <aside class="single_sidebar_widget popular_post_widget">
                            <div class="row_1" style="background-color: #ffffff">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <ul class="fa-ul">
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>

                                    </ul>
                                </div>

                            </div>
                        </aside>

                        <div class="row_1">
                            <p class="widget_title title screen"><span class="fa fa-check" style="color: #13c5fc"></span> Lesson 1</p>
                            <p class="widget_title title">1/7</p>

                        </div>

                        <aside class="single_sidebar_widget popular_post_widget">
                            <div class="row_1" style="background-color: #ffffff">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <ul class="fa-ul">
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                    </ul>
                                </div>

                            </div>
                        </aside>
                        <div class="row_1">
                            <p class="widget_title title screen"><span class="fa fa-check" style="color: #13c5fc"></span> Lesson 3</p>
                            <p class="widget_title title">0/5</p>

                        </div>

                        <aside class="single_sidebar_widget popular_post_widget">
                            <div class="row_1" style="background-color: #ffffff">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <ul class="fa-ul">
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                        <li><i class="fa-li fa fa-check-circle-o"></i>At narum lucus integre, anima facilis</li>
                                    </ul>

                                </div>
                            </div>
                        </aside>

                    </div>
                    <a href="{{route('quiz')}}" class="btn btn-primary cours_btn" style="padding-top:13px">  Cours suivant <i class="fa fa-chevron-right"></i></a>
                </div>
            </div>
        </div>
    </section>


@endsection