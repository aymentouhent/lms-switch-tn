<!Doctype html>
<html>
@include('Front.partials._head')
<body>
@include('Front.partials._header')

@yield('content')
@include('Front.partials._javacsripts')
</body>
</html>