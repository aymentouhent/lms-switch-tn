<script src="{{asset('SmartExpFront/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('SmartExpFront/js/popper.js')}}"></script>
<script src="{{asset('SmartExpFront/js/bootstrap.min.js')}}"></script>
<script src="{{asset('SmartExpFront/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('SmartExpFront/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('SmartExpFront/js/mail-script.js')}}"></script>
<script src="{{asset('SmartExpFront/js/mail-script.js')}}"></script>
<script src="{{asset('SmartExpFront/js/stellar.js')}}"></script>
<script src="{{asset('SmartExpFront/vendors/lightbox/simpleLightbox.min.js')}}"></script>
<script src="{{asset('SmartExpFront/vendors/flipclock/timer.js')}}"></script>
<script src="{{asset('SmartExpFront/vendors/nice-select/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('SmartExpFront/js/custom.js')}}"></script>
<script>
    $('#menu li a').click(function(e){
        e.preventDefault();
        $(this).parent().addClass('active').siblings().removeClass('active');
    });
</script>
@yield('scripts')