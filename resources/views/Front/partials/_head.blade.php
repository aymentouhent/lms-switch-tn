<meta charset="utf-8">
<title>SmartXpr</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" href="{{asset('SmartExpFront/image/Favicon_smartxpr.png')}}">
<link rel="stylesheet" href="{{asset('SmartExpFront/css/bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('SmartExpFront/vendors/linericon/style.css')}}">
<link rel="stylesheet" href="{{asset('SmartExpFront/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('SmartExpFront/vendors/owl-carousel/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('SmartExpFront/vendors/lightbox/simpleLightbox.css')}}">
<link rel="stylesheet" href="{{asset('SmartExpFront/vendors/nice-select/css/nice-select.css')}}">
<!-- main css -->
<link rel="stylesheet" href="{{asset('SmartExpFront/css/style.css')}}">
<link rel="stylesheet" href="{{asset('SmartExpFront/css/responsive.css')}}">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>

</style>
@yield('stylesheet')