<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


	@if(session()->has('success'))
				 
		<script type="text/javascript">
			toastr.success("{{session()->get('success')}}"); 
		</script>
	@endif
		

	@if(session()->has('error'))
		<script type="text/javascript">
			toastr.error("{{session()->get('error')}}"); 
		</script>
				 			
	@endif