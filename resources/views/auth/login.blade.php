@extends('auth.auth_stuff')
@section('content')
 <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group" style="padding:0px 14px">
                    <label style="font-size: 12px;color: #676767">Email :  </label>
                    <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback" style="padding:0px 14px">
                    <label  style="font-size: 12px;color: #676767">Mot de passe :  </label>
                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required >
                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group" style="padding:0px 14px">
                    <a href="{{ route('password.request') }}" style="color: #13c5fc;font-size: 12px;font-weight: bold">Mot de passe perdu?</a>
                </div>
                <button type="submit" class="btn btn-block btn-lg" style="background-color: #13c5fc;color: white;width: 100%;font-size: 12px;text-align: center;padding:20px 16px;border-radius: 0px!important;font-weight:200">Connexion</button>

</form>
@endsection