@extends('auth.auth_stuff')
@section('content')
 <form method="POST" action="{{ route('password.request') }}">
    @csrf

                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group" style="padding:0px 14px">
                    <label style="font-size: 12px;color: #676767">Email :  </label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                </div>

                <div class="form-group has-feedback" style="padding:0px 14px">
                    <label  style="font-size: 12px;color: #676767">Nouveau Mot de passe :  </label>
                   <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                </div>
                
                <div class="form-group has-feedback" style="padding:0px 14px">
                    <label  style="font-size: 12px;color: #676767">Confirmer votre mot de passe :  </label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                </div>
               

                <button type="submit" class="btn btn-block btn-lg" style="background-color: #13c5fc;color: white;width: 100%;font-size: 12px;text-align: center;padding:20px 16px;border-radius: 0px!important;font-weight:200">Réinitialiser
                </button>

</form>
@endsection